# -*- coding: utf-8 -*-
import click
import logging
from pathlib import Path
from dotenv import find_dotenv, load_dotenv
import pandas as pd
import numpy as np


@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
def modify_data(input_filepath: str, output_filepath: str):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    train = pd.read_csv(input_filepath)
    # Модель (оставлю только производителя)
    train = train.reset_index(drop=True)
    for i in range(train.shape[0]):
        train.at[i, 'Company'] = train['model'][i].split()[0]
    train.drop(["model"], axis=1, inplace=True)
    # Число записей о регистрации и в истории пробега
    train.loc[(train['Число записей о регистрации'] >= 20), 'Число записей о регистрации'] = 20
    train.loc[(train['Число записей в истории пробега'] >= 25), 'Число записей в истории пробега'] = 25
    # Коробка передач, привод, руль, тип двигателя (преобразую в числовые классы)
    train['Коробка передач'].replace({'робот': 1, 'вариатор': 2, 'механика': 3, 'АКПП': 4}, inplace=True)
    train['Привод'].replace({'передний': 1, '4WD': 2, 'задний': 3}, inplace=True)
    train['Руль'].replace({'левый': 1, 'правый': 2}, inplace=True)
    train['Тип двигателя'].replace({'бензин': 1, 'дизель': 2, 'электро': 3}, inplace=True)
    # Цвет (можно избавиться, исходя из графика)
    train.drop(['Цвет'], axis=1, inplace=True)
    # Чистка от выбросов (мощность, объем двигателя)
    train.loc[train['Мощность, л.с.'] > 1000, 'Мощность, л.с.'] = 60
    train[train['Объем двигателя, л'] > 7]
    # Посмотрел значения мощностей для данных моделей
    train.loc[train['Объем двигателя, л'] == 10, 'Объем двигателя, л'] = 1
    train.loc[train['Объем двигателя, л'] == 7.7, 'Объем двигателя, л'] = 1.5
    train.loc[train['Объем двигателя, л'] == 7.8, 'Объем двигателя, л'] = 2

    train.to_csv(output_filepath, index=False)


if __name__ == '__main__':
    '''
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())
    '''
    modify_data()
