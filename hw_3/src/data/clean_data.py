# -*- coding: utf-8 -*-
import click
import logging
from pathlib import Path
from dotenv import find_dotenv, load_dotenv
import pandas as pd
import numpy as np


@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
def clean_data(input_filepath: str, output_filepath: str):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    train = pd.read_excel(input_filepath).astype({'id': str})
    # Избавляюсь от признаков, которые не влияют на обучение модели
    train.drop(['title'], axis=1, inplace=True)
    train.drop(['city'], axis=1, inplace=True)
    train.drop(['region'], axis=1, inplace=True)
    train.drop(['description'], axis=1, inplace=True)
    train.drop(['Числится в розыске'], axis=1, inplace=True)
    train.drop(['Ограничений не обнаружено'], axis=1, inplace=True)
    train.drop(['Число предыдущих объявлений'], axis=1, inplace=True)
    train.drop(['Комплектация'], axis=1, inplace=True)
    train.drop(['VIN'], axis=1, inplace=True)
    train.drop(['Проверено'], axis=1, inplace=True)
    train.drop(['Номер кузова'], axis=1, inplace=True)
    # Если владелец не указал эти характеристики, то наверное он считает, что с ними все в порядке, поэтому выставляю лучшее значение
    train['Характеристики  совпадают с ПТС'] = train['Характеристики  совпадают с ПТС'].fillna(1.0)
    train['Число записей о регистрации'] = train['Число записей о регистрации'].fillna(1.0)
    train['Число записей в истории пробега'] = train['Число записей в истории пробега'].fillna(1.0)
    train['Поколение'] = train['Поколение'].fillna(1.0)
    # автомат перевожу в АКПП, т.к. это одно и то же. Пропущенные значения заменю на самые распространенные
    train['Коробка передач'] = np.where((train['Коробка передач'] == 'автомат'), 'АКПП', train['Коробка передач'])
    train['Коробка передач'] = train['Коробка передач'].fillna('АКПП')
    train['Пробег, км'] = train['Пробег, км'].fillna(train.groupby(['year', 'model'])['Пробег, км'].transform('median'))
    train.loc[(train['Новый автомобиль'] == 1), 'Пробег, км'] = 0
    train['Пробег, км'].fillna(round(train['Пробег, км'].mean(), 0), inplace=True)
    train['Пробег, км'].fillna(round(train['Пробег, км'].mean(), 0), inplace=True)
    train['Тип кузова'] = train['Тип кузова'].fillna('unknown')
    train['Цвет'] = train['Цвет'].fillna('белый')
    train['Руль'] = train['Руль'].fillna('левый')
    train['Привод'] = train['Привод'].fillna('передний')
    # Избавляюсь от дублей по комбинации признаков
    train.drop_duplicates(subset=['model', 'year', 'Пробег, км', 'Объем двигателя, л', 'price'], keep=False)

    train.to_csv(output_filepath, index=False)

if __name__ == '__main__':
    '''
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())
    '''
    clean_data()
